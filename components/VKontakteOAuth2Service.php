<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 03.03.16
 * Time: 12:27
 */

namespace app\components;

class VKontakteOAuth2Service extends \nodge\eauth\services\VKontakteOAuth2Service
{

    public function getServiceTitle()
    {
        return $this->title;
    }
} 