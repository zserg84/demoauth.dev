<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 03.03.16
 * Time: 12:28
 */

namespace app\components;


class FacebookOAuth2Service extends \nodge\eauth\services\FacebookOAuth2Service
{

    public function getServiceTitle()
    {
        return $this->title;
    }
} 