<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Приватная страница';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Приватная страница. Сюда могут попадать только авторизованные пользователи
    </p>

</div>
